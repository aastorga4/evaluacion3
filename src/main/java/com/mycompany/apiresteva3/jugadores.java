/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apiresteva3;

import com.mycompany.apiresteva3.dao.JugadoresJpaController;
import com.mycompany.apiresteva3.dao.exceptions.NonexistentEntityException;
import com.mycompany.apiresteva3.entity.Jugadores;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Alonso
 */
@Path("jugadores")

public class jugadores {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarSolicitudes(){
        /*Jugadores jg1= new Jugadores();
        jg1.setId("2");
        jg1.setNickname("nuevo");
        jg1.setNivel("3");
        jg1.setPremium("si");
        jg1.setTotalHoras("1");
        List<Jugadores> lista= new ArrayList<Jugadores>();
       lista.add(jg1);*/
        
       JugadoresJpaController dao= new JugadoresJpaController();
        List<Jugadores> lista= dao.findJugadoresEntities();
       return Response.ok(200).entity(lista).build();
    }
      @POST
    @Produces(MediaType.APPLICATION_JSON) 
    public Response add(Jugadores j){
       
       try {
            JugadoresJpaController dao= new JugadoresJpaController();
            dao.create(j);
        } catch (Exception ex) {
            Logger.getLogger(Jugadores.class.getName()).log(Level.SEVERE, null, ex);
        }
        
           return Response.ok(200).entity(j).build();
        
    }
    
     @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON) 
   public Response delete(@PathParam("iddelete") String iddelete){
       
        try {
            JugadoresJpaController dao= new JugadoresJpaController();
            
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Jugadores.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        return Response.ok("cliente eliminado").build();
    }
   
   
      @PUT
    public Response update(Jugadores j){
        
        try {
             JugadoresJpaController dao= new JugadoresJpaController();
            dao.edit(j);
       } catch (Exception ex) {
            Logger.getLogger(Jugadores.class.getName()).log(Level.SEVERE, null, ex);
       }
        
          return Response.ok(200).entity(j).build();
    }
    
    
    
}
